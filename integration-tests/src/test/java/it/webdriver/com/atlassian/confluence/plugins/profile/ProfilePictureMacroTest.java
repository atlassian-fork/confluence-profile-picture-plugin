package it.webdriver.com.atlassian.confluence.plugins.profile;

import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.test.utils.Classpath;
import com.atlassian.confluence.util.test.annotations.ExportedTestClass;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.MacroBrowserDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.MacroBrowserPreview;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.MacroForm;
import com.atlassian.confluence.webdriver.pageobjects.component.form.Autocomplete;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import com.atlassian.confluence.webdriver.pageobjects.page.user.EditUserAvatarPage;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_EDIT;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.VIEW;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

@ExportedTestClass
@RunWith(ConfluenceStatelessTestRunner.class)
public class ProfilePictureMacroTest {

    private static final String VALID_IMAGE_PATH = "images/valid-image.jpg";

    @Inject
    private static ConfluenceTestedProduct product;

    @Fixture
    private static UserFixture user = userFixture().build();

    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, VIEW, PAGE_EDIT)
            .build();

    @Fixture
    private static PageFixture page = pageFixture()
            .author(user).space(space).title("page")
            .build();

    @Test
    public void testAvatarSrcIsValid() {
        //CONFSRV-699
        UserWithDetails userDetails = user.get();

        EditUserAvatarPage editUserAvatarPage = product.login(userDetails, EditUserAvatarPage.class);
        editUserAvatarPage.uploadAndSetUserAvatar(new File(Classpath.getResource(VALID_IMAGE_PATH).getFile()));

        EditContentPage editedPage = product.loginAndEdit(userDetails, page.get());
        MacroBrowserDialog macroDialog = editedPage.getEditor().openMacroBrowser();
        MacroForm macroForm = macroDialog.searchForFirst("Profile Picture").select();
        macroForm.waitUntilVisible();

        Autocomplete suggestions = macroForm.getAutocompleteField("User").typeTextAndWaitForAutocomplete(userDetails.getUsername());
        suggestions.clickItemWithText(userDetails.getFullName());

        MacroBrowserPreview preview = macroDialog.preview();
        String imageSrc = preview.getContentWithinIframeByClassName("userLogo", content -> content.getAttribute("src"));
        // assert that the imageSrc contains only 1 occurrence of double-forward-slashes (the one within the protocol section)
        assertEquals(1, org.springframework.util.StringUtils.countOccurrencesOf(imageSrc, "//"));

        macroDialog.clickSave();
        waitUntilTrue(editedPage.getEditor().getContent().hasMacro(
                "profile-picture",
                singletonList("User=" + user.get().getUsername())
        ));
    }
}
